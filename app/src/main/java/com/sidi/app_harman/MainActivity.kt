package com.sidi.app_harman


import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sidi.app_harman.databinding.ActivityMainBinding

import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityMainBinding
    private lateinit var btnSign: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
    }

    override fun onStart() {
        super.onStart()
        btnSign = findViewById(R.id.btnsignin)

        _binding.btnsignin.setOnClickListener {
            if (_binding.edtuser.text.isNullOrBlank()&&edtpassword.text.isNullOrBlank()) {
                Toast.makeText(this, "Please fill the required fields", Toast.LENGTH_LONG).show()
            }else {
                startActivity(Intent(this, HomeActivity::class.java))
            }
        }
    }
}