package com.sidi.app_harman.data.models

data class Sensor(
    var name: String,
    var status: Boolean
)
