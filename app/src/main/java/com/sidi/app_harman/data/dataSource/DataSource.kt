package com.sidi.app_harman.data.dataSource

import com.sidi.app_harman.data.models.Sensor

class DataSource {

    companion object {
        fun createDataSet() : ArrayList<Sensor> {
            var list = ArrayList<Sensor>()

            list.add(
                Sensor(
                    name = "Engine",
                    status = false
                ),
            )

            list.add(
                Sensor(
                    name = "Geolocation",
                    status = true
                ),
            )

            list.add(
                Sensor(
                    name = "Bateries",
                    status = false
                ),
            )

            list.add(
                Sensor(
                    name = "Electronic Injection",
                    status = false
                ),
            )

            return list
        }
    }
}