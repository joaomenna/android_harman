package com.sidi.app_harman

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager
import com.amazonaws.regions.Regions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.sidi.app_harman.databinding.ActivityHomeBinding
import java.util.Timer


class HomeActivity : AppCompatActivity() {

    // --- Constants to modify per your configuration ---
    val DEFAULT_UPDATE_INTERVAL: Int = 10*1000
    val FAST_UPDATE_INTERVAL: Int = 5*1000
    val LOG_TAG: String = HomeActivity::class.qualifiedName.toString()

    // ------ AWS permissions and configurations  ------
    // Cognito identity pool ID for unauthenticated users
    val COGNITO_POOL_ID: String = "us-east-1:18a9b2b4-2a7b-45ad-92e3-73617a842bb0"

    // Region of AWS IoT
    val MY_REGION: Regions = Regions.US_EAST_1

    // Customer specific IoT endpoint
    val CUSTOMER_SPECIFIC_ENDPOINT: String =  "a2ptn29ehp7uom-ats.iot.us-east-1.amazonaws.com"

    // IoT thing name defined at AWS console
    val THING_NAME: String = "avl_01"

    // IoT thing device shadow MQTT topic
    val TOPIC: String = "aws/things$THING_NAME/shadow/update"

    val PERMISSION_REQUEST_ID: Int = 1

    lateinit var credentialsProvider: CognitoCachingCredentialsProvider
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallBack: LocationCallback
    lateinit var mqttManager: AWSIotMqttManager

    private lateinit var timer: Timer


    private val binding: ActivityHomeBinding by lazy {
        ActivityHomeBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupNavigationDrawer()
        connectServerMqtt()
    }

    private fun connectServerMqtt() {
        // TODO
    }

    private fun setupNavigationDrawer() {
        // Pega a referência do navHost no layout xml
        val navHostFragment = supportFragmentManager
            .findFragmentById(binding.fragmentContainerView.id) as NavHostFragment

        // Pega o controller relacionado ao navHostFragment
        val navController = navHostFragment.navController

        binding.navView.setupWithNavController(navController = navController)

        // Seta na configuração da toolbar o gráfico de navegação que fará todo o controle
        // da toolbar com o Navigation Component e passando a o drawerLayout para trabalhar
        // junto com o navigation

        val appBarConfiguration = AppBarConfiguration(
            navGraph = navController.graph,
            drawerLayout = binding.drawerLayout
        )

        // Seta a toolbar com o navController e a configuração da toolbar com o gráfico
        binding.navigationDrawerToolbar.setupWithNavController(
            navController = navController,
            configuration = appBarConfiguration
        )
    }
}

private fun AWSIotMqttManager.connect(credentialsProvider: CognitoCachingCredentialsProvider, function: () -> Unit) {

}
