package com.sidi.app_harman.ui.vehicle

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.sidi.app_harman.R
import com.sidi.app_harman.data.dataSource.DataSource
import com.sidi.app_harman.databinding.FragmentVehicleBinding
import com.sidi.app_harman.ui.adapters.SensorAdapter
import kotlinx.android.synthetic.main.fragment_vehicle.*
import kotlinx.android.synthetic.main.item_sensor.*


class VehicleFragment : Fragment() {

    private lateinit var _binding: FragmentVehicleBinding
    private lateinit var sensorAdapter: SensorAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentVehicleBinding.inflate(inflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        addDataSource()
    }

    private fun addDataSource() {

        val dataSource = DataSource.createDataSet()
        this.sensorAdapter.setDataSet(dataSource)
    }

    private fun initRecyclerView() {
        sensorAdapter = SensorAdapter {
            if (it.name.equals("Geolocation")) {
                findNavController().navigate(R.id.FromVehicleFragmentToDetailsFragment)
            }
        }

        recyclerViewSensors.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = sensorAdapter
        }

    }


}