package com.sidi.app_harman.ui.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sidi.app_harman.R
import com.sidi.app_harman.data.models.Sensor
import kotlinx.android.synthetic.main.item_sensor.view.*

class SensorAdapter(
    private val onItemClicked: (Sensor) -> Unit
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<Sensor> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SensorViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_sensor, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is SensorViewHolder -> {
                holder.bind(items[position], onItemClicked)
            }
        }

    }

    override fun getItemCount(): Int {
       return items.size
    }

    fun setDataSet(sensor: List<Sensor>) {
        this.items = sensor
    }

    class SensorViewHolder constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView) {

        private val sensorName = itemView.itemNameSensor
        private val status = itemView.itemStatus

        fun bind(sensor: Sensor, onItemClicked: (Sensor) -> Unit) {
            sensorName.text = sensor.name
            status.text = sensor.status.toString()

            if (status.text == "true") {
                status.setTextColor(Color.GREEN)
            }
            itemView.setOnClickListener {
                onItemClicked(sensor)
            }
        }
    }

}